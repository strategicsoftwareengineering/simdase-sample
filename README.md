# Structured Intuitive Model for Dynamic Adaptive System Economics (SIMDASE) Examples

This repository holds examples for the SIMDASE cost estimation tooling for the OSATE IDE.

## Installation

Install the [SIMDASE annex](https://bitbucket.org/strategicsoftwareengineering/simdase-osate) in OSATE first, then import these examples.

## Contributions

Contributions, in general, are not accepted to this repository.  If you are unsure, please contact the [primary investigator](mailto:etmcgee@g.clemson.edu) before making the pull request.  Pull requests will be merged after they have been reviewed.

## License

Copyright 2017 Clemson University

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.